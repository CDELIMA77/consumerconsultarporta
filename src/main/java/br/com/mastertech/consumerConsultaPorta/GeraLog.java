package br.com.mastertech.consumerConsultaPorta;

import br.com.mastertech.producerConsultaPorta.Autorizacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class GeraLog {

    @Autowired
    WriteToFile writeToFile;

    @KafkaListener(topics = "spec2-cynthia-carvalho-1", groupId = "grupo-1")
    public void receber(@Payload Autorizacao autorizacao) {
        String resultado ;
        String linhaLog ;
        if (autorizacao.isAutoriza()) {resultado = "S";} else {resultado = "N";}
        linhaLog = ("Cliente " + autorizacao.getCliente() + " consultou se tem autorizacao a porta " + autorizacao.getPorta() + " e o resultado foi :" + resultado);
        WriteToFile.gravaLog(linhaLog);
    }
}