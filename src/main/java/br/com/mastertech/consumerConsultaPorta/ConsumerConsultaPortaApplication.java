package br.com.mastertech.consumerConsultaPorta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerConsultaPortaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerConsultaPortaApplication.class, args);
	}

}

